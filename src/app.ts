import angular from 'angular';
import uiRouter from 'angular-ui-router';
import { ShoppingCartComponent } from './shopping-cart.component';
import { ShoppingListComponent } from './shopping-list.component';

export interface Product {
    product: string,
    price: number,
    checked: boolean
}

export class CartService {
    static $inject: string[] = ['$http'];
    protected products: Product[] = [];

    constructor(protected $http: angular.IHttpService) {
        this.fetchProducts();
    }

    private fetchProducts(): any {
        return this.$http.get('src/products.json')
            .then( (response: angular.IHttpPromiseCallbackArg<any>) => {
                this.products = response.data.map(obj => {
                    obj.checked = false;
                    return obj;
                });
            });
    }

    public setCheckedProducts(products: Product[]): void {
        for(let i = 0; i<this.products.length; i++) {
            this.products[i].checked = products[i].checked;
        }
    }

    public getProducts(): Product[] {
        return this.products;
    }
}

export const AppComponent: angular.IComponentOptions = {
    bindings: {
        products: '<',
        onViewChange: '&'
    },
    template: `
       <div class="card">
            <div class="card-header">
                <h2>My Shopping List</h2>
                <ul class="btn--group">
                    <li class="btn"><a ui-sref="home" ui-sref-active="active">Home</a></li>
                    <li class="btn"><a ui-sref="shoppingList" ui-sref-active="active">Items List</a></li>
                </ul>
                <span class="hr"></span>
            </div>
                
            <div class="card-body">
                <div ui-view></div>
            </div>
        </div>
    `
};

export const ShoppingModule = angular
    .module('shopping', [ uiRouter ])
    .component('shoppingCart', ShoppingCartComponent)
    .component('shoppingList', ShoppingListComponent)
    .service('CartService', CartService)
    .config(($stateProvider: angular.ui.IStateProvider,
             $urlRouterProvider: angular.ui.IUrlRouterProvider) => {
        $stateProvider
            .state('home', {
                name: 'home',
                url: '/home',
                component: 'shoppingCart',
            })
            .state( 'shoppingList', {
                name: 'shoppingList',
                url: '/productsInCart-list',
                component: 'shoppingList',
            });
        $urlRouterProvider.otherwise('/home');
    })
    .name;
