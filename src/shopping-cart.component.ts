import angular from 'angular';
import { Product, CartService } from "./app";

export class ShoppingCartController {
    static $inject: string[] = ['CartService'];
    protected productsInCart: Product[] = [];

    constructor(private CartService: CartService) {
        this.productsInCart = this.CartService.getProducts().filter(sL => sL.checked);
    }

    public isNonEmpty(): boolean {
        return this.productsInCart.length > 0;
    }

    public isEmpty(): boolean {
        return !this.isNonEmpty();
    }

    public checkoutSum(): number {
        if(this.isNonEmpty()){
            return this.productsInCart
                .map(el => el.price)
                .reduce((prev: number, curr: number) => {
                    return prev + curr;
                });
        }
        return 0;
    }
}

export const ShoppingCartComponent: angular.IComponentOptions = {
    controller: ShoppingCartController,
    template: `
        <div>
            <span ng-show="$ctrl.isEmpty()">Your list is empty...</span>
            <table ng-show="$ctrl.isNonEmpty()">
                <tr ng-repeat="item in $ctrl.productsInCart" >
                    <td><span class="product-name">{{item.product}}</span></td>
                    <td><span class="product-price">{{item.price + " zł"}}</span></td>
                </tr>
                
                
            </table>
            <div ng-show="$ctrl.isNonEmpty()" id="sum-up">
                <span>
                    <span id="sum">Sum</span>
                    <span id="price">{{$ctrl.checkoutSum() + " zł"}}</span>
                </span>
            </div>
        </div>    
`
};