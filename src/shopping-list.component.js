System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ShoppingListController, ShoppingListComponent;
    return {
        setters:[],
        execute: function() {
            ShoppingListController = (function () {
                function ShoppingListController(CartService) {
                    this.CartService = CartService;
                    this.shoppingList = [];
                    this.shoppingList = this.CartService.getProducts();
                }
                ShoppingListController.prototype.syncCart = function (checked, item) {
                    for (var i = 0; i < this.shoppingList.length; i++) {
                        if (this.shoppingList[i].product === item.product) {
                            this.shoppingList[i].checked = checked;
                        }
                    }
                    this.CartService.setCheckedProducts(this.shoppingList);
                };
                ShoppingListController.$inject = ['CartService'];
                return ShoppingListController;
            }());
            exports_1("ShoppingListController", ShoppingListController);
            exports_1("ShoppingListComponent", ShoppingListComponent = {
                controller: ShoppingListController,
                bindings: {
                    shoppingList: '<',
                },
                template: "\n        <table>\n            <tr ng-repeat=\"item in $ctrl.shoppingList\" >\n                <td><label tabindex=\"1\" for=\"check-{{item.product}}\"><span class=\"product-name\">{{item.product}}</span></label></td>\n                <td><label tabindex=\"1\" for=\"check-{{item.product}}\"><span class=\"product-price\">{{item.price + \" z\u0142\"}}</span></label></td>\n                <td class=\"checkbox\" >\n                    <input type=\"checkbox\" id=\"check-{{item.product}}\"  \n                        ng-model=\"checked\"\n                        ng-init=\"item.checked\"\n                        ng-checked=\"item.checked\"\n                        ng-click=\"$ctrl.syncCart(checked, item)\">\n                    <span class=\"checkbox-fake\"></span>\n                </td>\n            </tr>\n        </table>\n    ",
            });
        }
    }
});
//# sourceMappingURL=shopping-list.component.js.map