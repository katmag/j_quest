System.register(['angular', 'angular-ui-router', './shopping-cart.component', './shopping-list.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var angular_1, angular_ui_router_1, shopping_cart_component_1, shopping_list_component_1;
    var CartService, AppComponent, ShoppingModule;
    return {
        setters:[
            function (angular_1_1) {
                angular_1 = angular_1_1;
            },
            function (angular_ui_router_1_1) {
                angular_ui_router_1 = angular_ui_router_1_1;
            },
            function (shopping_cart_component_1_1) {
                shopping_cart_component_1 = shopping_cart_component_1_1;
            },
            function (shopping_list_component_1_1) {
                shopping_list_component_1 = shopping_list_component_1_1;
            }],
        execute: function() {
            CartService = (function () {
                function CartService($http) {
                    this.$http = $http;
                    this.products = [];
                    this.fetchProducts();
                }
                CartService.prototype.fetchProducts = function () {
                    var _this = this;
                    return this.$http.get('src/products.json')
                        .then(function (response) {
                        _this.products = response.data.map(function (obj) {
                            obj.checked = false;
                            return obj;
                        });
                    });
                };
                CartService.prototype.setCheckedProducts = function (products) {
                    for (var i = 0; i < this.products.length; i++) {
                        this.products[i].checked = products[i].checked;
                    }
                };
                CartService.prototype.getProducts = function () {
                    return this.products;
                };
                CartService.$inject = ['$http'];
                return CartService;
            }());
            exports_1("CartService", CartService);
            exports_1("AppComponent", AppComponent = {
                bindings: {
                    products: '<',
                    onViewChange: '&'
                },
                template: "\n       <div class=\"card\">\n            <div class=\"card-header\">\n                <h2>My Shopping List</h2>\n                <ul class=\"btn--group\">\n                    <li class=\"btn\"><a ui-sref=\"home\" ui-sref-active=\"active\">Home</a></li>\n                    <li class=\"btn\"><a ui-sref=\"shoppingList\" ui-sref-active=\"active\">Items List</a></li>\n                </ul>\n                <span class=\"hr\"></span>\n            </div>\n                \n            <div class=\"card-body\">\n                <div ui-view></div>\n            </div>\n        </div>\n    "
            });
            exports_1("ShoppingModule", ShoppingModule = angular_1.default
                .module('shopping', [angular_ui_router_1.default])
                .component('shoppingCart', shopping_cart_component_1.ShoppingCartComponent)
                .component('shoppingList', shopping_list_component_1.ShoppingListComponent)
                .service('CartService', CartService)
                .config(function ($stateProvider, $urlRouterProvider) {
                $stateProvider
                    .state('home', {
                    name: 'home',
                    url: '/home',
                    component: 'shoppingCart',
                })
                    .state('shoppingList', {
                    name: 'shoppingList',
                    url: '/productsInCart-list',
                    component: 'shoppingList',
                });
                $urlRouterProvider.otherwise('/home');
            })
                .name);
        }
    }
});
//# sourceMappingURL=app.js.map