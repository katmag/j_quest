import angular from 'angular';
import { CartService, Product } from './app';

export class ShoppingListController {
    static $inject: string[] = ['CartService'];
    private shoppingList: Product[] = [];

    constructor(private CartService: CartService) {
        this.shoppingList = this.CartService.getProducts();
    }

    public syncCart(checked: boolean, item: Product): void {
        for (let i=0; i < this.shoppingList.length; i++) {
            if (this.shoppingList[i].product === item.product) {
                this.shoppingList[i].checked = checked;
            }
        }
        this.CartService.setCheckedProducts(this.shoppingList);
    }
}

export const ShoppingListComponent: angular.IComponentOptions = {
    controller: ShoppingListController,
    bindings: {
        shoppingList: '<',
    },
    template: `
        <table>
            <tr ng-repeat="item in $ctrl.shoppingList" >
                <td><label tabindex="1" for="check-{{item.product}}"><span class="product-name">{{item.product}}</span></label></td>
                <td><label tabindex="1" for="check-{{item.product}}"><span class="product-price">{{item.price + " zł"}}</span></label></td>
                <td class="checkbox" >
                    <input type="checkbox" id="check-{{item.product}}"  
                        ng-model="checked"
                        ng-init="item.checked"
                        ng-checked="item.checked"
                        ng-click="$ctrl.syncCart(checked, item)">
                    <span class="checkbox-fake"></span>
                </td>
            </tr>
        </table>
    `,
};

