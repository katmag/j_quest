import angular from 'angular';
import {AppComponent, ShoppingModule} from './app';

let appModule =
    angular.module('app', [ ShoppingModule ])
        .component('myApp', AppComponent);

angular.bootstrap(document, [ appModule.name ]);