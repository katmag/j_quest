System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ShoppingCartController, ShoppingCartComponent;
    return {
        setters:[],
        execute: function() {
            ShoppingCartController = (function () {
                function ShoppingCartController(CartService) {
                    this.CartService = CartService;
                    this.productsInCart = [];
                    this.productsInCart = this.CartService.getProducts().filter(function (sL) { return sL.checked; });
                }
                ShoppingCartController.prototype.isNonEmpty = function () {
                    return this.productsInCart.length > 0;
                };
                ShoppingCartController.prototype.isEmpty = function () {
                    return !this.isNonEmpty();
                };
                ShoppingCartController.prototype.checkoutSum = function () {
                    if (this.isNonEmpty()) {
                        return this.productsInCart
                            .map(function (el) { return el.price; })
                            .reduce(function (prev, curr) {
                            return prev + curr;
                        });
                    }
                    return 0;
                };
                ShoppingCartController.$inject = ['CartService'];
                return ShoppingCartController;
            }());
            exports_1("ShoppingCartController", ShoppingCartController);
            exports_1("ShoppingCartComponent", ShoppingCartComponent = {
                controller: ShoppingCartController,
                template: "\n        <div>\n            <span ng-show=\"$ctrl.isEmpty()\">Your list is empty...</span>\n            <table ng-show=\"$ctrl.isNonEmpty()\">\n                <tr ng-repeat=\"item in $ctrl.productsInCart\" >\n                    <td><span class=\"product-name\">{{item.product}}</span></td>\n                    <td><span class=\"product-price\">{{item.price + \" z\u0142\"}}</span></td>\n                </tr>\n                \n                \n            </table>\n            <div ng-show=\"$ctrl.isNonEmpty()\" id=\"sum-up\">\n                <span>\n                    <span id=\"sum\">Sum</span>\n                    <span id=\"price\">{{$ctrl.checkoutSum() + \" z\u0142\"}}</span>\n                </span>\n            </div>\n        </div>    \n"
            });
        }
    }
});
//# sourceMappingURL=shopping-cart.component.js.map