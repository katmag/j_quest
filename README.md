# Create two components: Shopping List and Shopping Cart.
# Shopping list should contain multiple items, their prices and a checkbox.
# Whenever a checkbox will get checked, the item should appear in Shopping Cart.
# Shopping cart should show sum of items prices.
# You should use Angular 1.5 and TypeScript for this task.